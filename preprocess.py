import os
import pickle

def getDictionary(directoryName):
   wordBucket = ""
   for filename in os.listdir(directory):
      filename = directoryName + '/' + filename
      fileData = open(filename, 'r').read()
      wordBucket += fileData
   wordList = wordBucket.split()
   return list(set(wordList))

def fileCount(directoryName):
   count = 0
   for filename in os.listdir(directory):
      count += 1
   return count

def fileLength(filename):
   i = 0
   with open(filename) as f:
      for line in f.readlines():
         splitLine = line.split()
         i += len(splitLine)
   return i

def wordCount(directoryName):
   maxCount = 0
   for filename in os.listdir(directory):
      length = fileLength(directoryName + "/" + filename)
      if length > maxCount:
         maxCount = length
   return maxCount

def addSpecialTokens(dictionary):
   dictionary.append('<eol>')       #End-of-line token
   dictionary.append('<eob>')       #End-of-block token
   dictionary.append('<eos>')       #End-of-song token
   return dictionary

def getConvertedRepresentations(dictionary):
   str_to_ix = { st:i for i,st in enumerate(dictionary) }
   ix_to_str = { i:st for i,st in enumerate(dictionary) }
   return str_to_ix, ix_to_str

def getInputRepresentation(directoryName, n_x):
   x = np.zeros(())
   for filename in os.listdir(directory):
      with open(filename) as f:

if __name__ == "__main__":
   directory = input("Enter relative location of input lyrics files (default is \"data/song_lyrics\"): ")
   if(directory == None or directory == ""):
      directory = "data/song_lyrics"
   print("Determining dictionary size...")
   dictionary = sorted(getDictionary(directory))
   dictionary = addSpecialTokens(dictionary)
   str_to_ix, ix_to_str = getConvertedRepresentations(dictionary)
   inputRep = getInputRepresentation(directoryName)
   with open("intermediate/str_to_ix.bin", "w+b") as fp:
      pickle.dump(str_to_ix, fp)
   with open("intermediate/ix_to_str.bin", "w+b") as fp:
      pickle.dump(ix_to_str, fp)
   with open("intermediate/input_rep.bin", "w+b") as fp:
      pickle.dump(inputRep, fp)
   maxWords = wordCount(directory)
   files = fileCount(directory)
