import os
import numpy as np
import tensorflow as tf
import pickle
from util import *
from keras.models import Model
import keras.backend as K

def optimize(X, Y, a_prev, parameters, learning_rate = 0.01):
    loss, cache = rnn_forward(X, Y, a_prev, parameters)
    gradients, a = rnn_backward(X, Y, parameters, cache)
    gradients = clip(gradients, 5)
    parameters = update_parameters(parameters, gradients, learning_rate)
    return loss, gradients, a[len(X)-1]

if __name__ == '__main__':
    str_to_ix, ix_to_str = None, None
    n_a, n_x, n_y = None, None, None
    vocab_size = len(str_to_ix)
    parameters = None
    loop = True

    while(loop):
        command = input("command: ")
        if(command == "i" or command == "init"):
            with open ('intermediate/str_to_ix.bin', 'rb') as fp:
                str_to_ix = pickle.load(fp)
            with open ('intermediate/ix_to_str.bin', 'rb') as fp:
                ix_to_str = pickle.load(fp)
            numUnits = int(input("Enter desired number of units for the network (Default is 50): "))
            if(numUnits == None or numUnits == 0):
                numUnits = 50
            n_a = numUnits
            n_x = len(str_to_ix)
            n_y = len(str_to_ix)
        elif(command == "t" or command == "train"):
            numIterations = int(input("Number of iterations: "))
            parameters = initialize_parameters(n_a, n_x, n_y)
            loss = get_initial_loss(vocab_size, 5)
        elif(command == "p" or command == "print"):
            print(ix_to_str)
            print(str_to_ix)
        elif(command == "q" or command == "quit"):
            loop = False
